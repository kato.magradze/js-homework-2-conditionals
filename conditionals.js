//ნეო
const neoFirstSubject = 60.2;
const neoSecondSubject = 57.3;
const neoThirdSubject = 72.4;
const neoFourthSubject = 88;

//ინდიანა
const indianaFirstSubject = 78.2;
const indianaSecondSubject = 52.3;
const indianaThirdSubject = 66.4;
const indianaFourthSubject = 80;

//სევერუსა
const severusFirstSubject = 75.2;
const severusSecondSubject = 67.3;
const severusThirdSubject = 54.4;
const severusFourthSubject = 90;

//ალადინი
const aladdinFirstSubject = 80.2;
const aladdinSecondSubject = 52.3;
const aladdinThirdSubject = 68.4;
const aladdinFourthSubject = 76;


//Neo Results
let neoTotalPoints = 0;
neoTotalPoints = neoFirstSubject + neoSecondSubject + neoThirdSubject + neoFourthSubject;

//Indiana Results 
let indianaTotalPoints = 0;
indianaTotalPoints = indianaFirstSubject + indianaSecondSubject + indianaThirdSubject + indianaFourthSubject;

//Severus Results
let severusTotalPoints = 0;
severusTotalPoints = severusFirstSubject + severusSecondSubject + severusThirdSubject + severusFourthSubject;

//Aladdin Results
let aladdinTotalPoints = 0;
aladdinTotalPoints = aladdinFirstSubject + aladdinSecondSubject + aladdinThirdSubject + aladdinFourthSubject;


/********************************************************************************/
//Highest score (with if/else)

let bestByPoints = '';

if ((neoTotalPoints > indianaTotalPoints) && (neoTotalPoints > severusTotalPoints) && (neoTotalPoints > aladdinTotalPoints)) {
    bestByPoints = 'Neo';
}
else if ((indianaTotalPoints > neoTotalPoints) && (indianaTotalPoints > severusTotalPoints) && (indianaTotalPoints > aladdinTotalPoints)) {
    bestByPoints = 'Indiana';
}
else if ((severusTotalPoints > neoTotalPoints) && (severusTotalPoints > indianaTotalPoints) && (severusTotalPoints > aladdinTotalPoints)) {
    bestByPoints = 'Severus';
}
else if ((aladdinTotalPoints > neoTotalPoints) && (aladdinTotalPoints > severusTotalPoints) && (aladdinTotalPoints > indianaTotalPoints)) {
    bestByPoints = 'Aladdin';
}
else {
    bestByPoints = 'Two or more students got the highest score.';
}

// console.log("BEST STUDENT: " + bestByPoints); 


/*********************************************************************************/
//Highest score (with switch)

switch(true) {
    case ((neoTotalPoints > indianaTotalPoints) && (neoTotalPoints > severusTotalPoints) && (neoTotalPoints > aladdinTotalPoints) ):
        bestByPoints = 'Neo';
        break;
    case ((indianaTotalPoints > neoTotalPoints) && (indianaTotalPoints > severusTotalPoints) && (indianaTotalPoints > aladdinTotalPoints)):
        bestByPoints = 'Indiana';
        break;
    case ((severusTotalPoints > neoTotalPoints) && (severusTotalPoints > indianaTotalPoints) && (severusTotalPoints > aladdinTotalPoints) ):
        bestByPoints = 'Severus';
        break;
    case ((aladdinTotalPoints > neoTotalPoints) && (aladdinTotalPoints > severusTotalPoints) && (aladdinTotalPoints > indianaTotalPoints) ):
        bestByPoints = 'Aladdin';
        break;
    default:
        bestByPoints = 'Two or more students got the highest score.';
}

// console.log("BEST STUDENT: " + bestByPoints);


/*************************************************************************/
//GPA Calculation

//Credits
const firstSubjectCredit = 4;
const secondSubjectCredit = 2;
const thirdSubjectCredit = 7;
const fourthSubjectCredit = 5;

const sumOfCredits = firstSubjectCredit + secondSubjectCredit + thirdSubjectCredit + fourthSubjectCredit;

//Neo's GPA
const neoFirstSubjectGPA = 0.5;
const neoSecondSubjectGPA = 0.5;
const neoThirdSubjectGPA = 2;
const neoFourthSubjectGPA = 3;


const neoFirstAvgGPA = neoFirstSubjectGPA * firstSubjectCredit;
const neoSecondAvgGPA = neoSecondSubjectGPA * secondSubjectCredit;
const neoThirdAvgGPA = neoThirdSubjectGPA * thirdSubjectCredit;
const neoFourthAvgGPA = neoFourthSubjectGPA * fourthSubjectCredit;

const neoFinalGPA = (neoFirstAvgGPA + neoSecondAvgGPA + neoThirdAvgGPA + neoFourthAvgGPA) / sumOfCredits;
// console.log("Neo's final GPA is: " + neoFinalGPA);


//Indiana's GPA
const indianaFirstSubjectGPA = 2;
const indianaSecondSubjectGPA = 0.5;
const indianaThirdSubjectGPA = 1;
const indianaFourthSubjectGPA = 2;

const indianaFirstAvgGPA = indianaFirstSubjectGPA * firstSubjectCredit;
const indianaSecondAvgGPA = indianaSecondSubjectGPA * secondSubjectCredit;
const indianaThirdAvgGPA = indianaThirdSubjectGPA * thirdSubjectCredit;
const indianaFourthAvgGPA = indianaFourthSubjectGPA * fourthSubjectCredit;

const indianaFinalGPA = (indianaFirstAvgGPA + indianaSecondAvgGPA + indianaThirdAvgGPA + indianaFourthAvgGPA) / sumOfCredits;
// console.log("Indiana's final GPA is: " + indianaFinalGPA);


//Severus' GPA
const severusFirstSubjectGPA = 2;
const severusSecondSubjectGPA = 1;
const severusThirdSubjectGPA = 0.5;
const severusFourthSubjectGPA = 3;

const severusFirstAvgGPA = severusFirstSubjectGPA * firstSubjectCredit;
const severusSecondAvgGPA = severusSecondSubjectGPA * secondSubjectCredit;
const severusThirdAvgGPA = severusThirdSubjectGPA * thirdSubjectCredit;
const severusFourthAvgGPA = severusFourthSubjectGPA * fourthSubjectCredit;

const severusFinalGPA = (severusFirstAvgGPA + severusSecondAvgGPA + severusThirdAvgGPA + severusFourthAvgGPA) / sumOfCredits;
// console.log("Severus' final GPA is: " + severusFinalGPA);


//Aladdin's GPA
const aladdinFirstSubjectGPA = 2;
const aladdinSecondSubjectGPA = 0.5;
const aladdinThirdSubjectGPA = 1;
const aladdinFourthSubjectGPA = 2;

const aladdinFirstAvgGPA = aladdinFirstSubjectGPA * firstSubjectCredit;
const aladdinSecondAvgGPA = aladdinSecondSubjectGPA * secondSubjectCredit;
const aladdinThirdAvgGPA = aladdinThirdSubjectGPA * thirdSubjectCredit;
const aladdinFourthAvgGPA = aladdinFourthSubjectGPA * fourthSubjectCredit;

const aladdinFinalGPA = (aladdinFirstAvgGPA + aladdinSecondAvgGPA + aladdinThirdAvgGPA + aladdinFourthAvgGPA) / sumOfCredits;
// console.log("Aladdin's final GPA is: " + aladdinFinalGPA);


/*********************************************************************************/
//Highest GPA (with if/else)

let bestByGPA = '';

if ((neoFinalGPA > indianaFinalGPA) && (neoFinalGPA > severusFinalGPA) && (neoFinalGPA > aladdinFinalGPA)) {
    bestByGPA = 'Neo';
}
else if ((indianaFinalGPA > neoFinalGPA) && (indianaFinalGPA > severusFinalGPA) && (indianaFinalGPA > aladdinFinalGPA)) {
    bestByGPA = 'Indiana';
}
else if ((severusFinalGPA > neoFinalGPA) && (severusFinalGPA > indianaFinalGPA) && (severusFinalGPA > aladdinFinalGPA) ) {
    bestByGPA = 'Severus';
}
else if ((aladdinFinalGPA > neoFinalGPA) && (aladdinFinalGPA > severusFinalGPA) && (aladdinFinalGPA > indianaFinalGPA) ) {
    bestByGPA = 'Aladdin';
}
else {
    bestByGPA = 'Two or more students got the highest GPA.';
}

// console.log("STUDENT WITH BEST GPA: " + bestByGPA);


/*********************************************************************************/
//Highest GPA (with switch)

switch(true) {
    case ((neoFinalGPA > indianaFinalGPA) && (neoFinalGPA > severusFinalGPA) && (neoFinalGPA > aladdinFinalGPA)):
        bestByGPA = 'Neo';
        break;
    case ((indianaFinalGPA > neoFinalGPA) && (indianaFinalGPA > severusFinalGPA) && (indianaFinalGPA > aladdinFinalGPA)):
        bestByGPA = 'Indiana';
        break;
    case ((severusFinalGPA > neoFinalGPA) && (severusFinalGPA > indianaFinalGPA) && (severusFinalGPA > aladdinFinalGPA)):
        bestByGPA = 'Severus';
        break;
    case ((aladdinFinalGPA > neoFinalGPA) && (aladdinFinalGPA > severusFinalGPA) && (aladdinFinalGPA > indianaFinalGPA)):
        bestByGPA: 'Aladdin';
        break;
    default: 
        bestByGPA = 'Two or more students got the highest GPA';
}

// console.log("STUDENT WITH BEST GPA: " + bestByGPA);
